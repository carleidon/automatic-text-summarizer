// Summarizer function
function textSummarizer() {
	document.getElementById('opText').textContent = 'Summarization in progress...';

	let inputText = document.getElementById('ipText').value;
	let lines = document.getElementById('numberOfLines').value;
	if (lines < 0 || lines === null || lines === undefined) {
		lines = Math.ceil(Math.sqrt(parseInt(countLines()))); // Input fallback
	}

	(async () => {
		const rawResponse = await fetch(
			'https://mltextsummarizer.azurewebsites.net/summarize',
			{
				method: 'POST',
				headers: {
					Accept: 'application/json',
					'Content-Type': 'application/json',
				},
				body: JSON.stringify({
					data: inputText,
					output_size: lines,
				}),
			}
		);
		const content = await rawResponse.text();

		document.getElementById('opText').textContent = content;
		netWordCount();
		netCountLines();
	})();
}

const onSubmit = document.getElementById('ipButton');
if (onSubmit) {
	onSubmit.addEventListener('click', textSummarizer, false);
}

// TextArea Automatic Expansion
function getScrollHeight(elm) {
	let savedValue = elm.value;
	elm.value = '';
	elm._baseScrollHeight = elm.scrollHeight;
	elm.value = savedValue;
}

function onExpandableTextareaInput({ target: elm }) {
	if (!elm.classList.contains('autoExpand') || !elm.nodeName == 'TEXTAREA')
		return;

	let minRows = elm.getAttribute('data-min-rows') | 0,
		rows;
	!elm._baseScrollHeight && getScrollHeight(elm);

	elm.rows = minRows;
	rows = Math.ceil((elm.scrollHeight - elm._baseScrollHeight) / 16);
	elm.rows = minRows + rows;
}

document.addEventListener('input', onExpandableTextareaInput);

// TextArea Word Count
document.querySelector('#ipText').addEventListener('keyup', wordCount);

function wordCount() {
	let resultArray = [];
	let inputText = document.getElementById('ipText').value;
	let str = inputText.replace(/[\t\n\r\.\?\!]/gm, ' ');
	let wordArray = str.split(' ');
	for (let i = 0; i < wordArray.length; i++) {
		let item = wordArray[i].trim();
		if (item.length > 0) {
			resultArray.push(item);
		}
	}
	if (resultArray.length === 1) {
		document.querySelector('#wordCount>em').innerText =
			resultArray.length + ' word';
	} else {
		document.querySelector('#wordCount>em').innerText =
			resultArray.length + ' words';
	}
}

function netWordCount() {
	let resultArray = [];
	let outputText = document.getElementById('opText').value;
	let str = outputText.replace(/[\t\n\r\.\?\!]/gm, ' ');
	let wordArray = str.split(' ');
	for (let i = 0; i < wordArray.length; i++) {
		let item = wordArray[i].trim();
		if (item.length > 0) {
			resultArray.push(item);
		}
	}
	if (resultArray.length === 1) {
		document.querySelector('#netWordCount>em').innerText =
			resultArray.length + ' word';
	} else {
		document.querySelector('#netWordCount>em').innerText =
			resultArray.length + ' words';
	}
}

// File upload
document.getElementById('inputFile').addEventListener('change', getFile);

function getFile(event) {
	const input = event.target;
	if ('files' in input && input.files.length > 0) {
		document.getElementById('ipText').value = '';
		placeFileContent(document.getElementById('ipText'), input.files[0]);
		document.getElementById('inputFile').value = '';
	}
}

function placeFileContent(target, file) {
	readFileContent(file)
		.then((content) => {
			target.value = content;
			wordCount();
			countLines();
		})
		.catch((error) => console.log(error));
}

function readFileContent(file) {
	const reader = new FileReader();
	return new Promise((resolve, reject) => {
		reader.onload = (event) => resolve(event.target.result);
		reader.onerror = (error) => reject(error);
		reader.readAsText(file);
	});
}

// Textarea line count
document.querySelector('#ipText').addEventListener('keyup', countLines);

function countLines() {
	let inputText = document.getElementById('ipText').value;
	const count = inputText.split(/[.?!]+/).length;
	if (count === 1) {
		if (inputText === '') {
			document.querySelector('#lineCount>em').innerText = '0 lines';
		} else {
			document.querySelector('#lineCount>em').innerText = count + ' line';
		}
	} else {
		document.querySelector('#lineCount>em').innerText = count - 1 + ' lines';
	}
	return count;
}

function netCountLines() {
	let inputText = document.getElementById('opText').value;
	const count = inputText.split(/[.?!]+/).length;
	if (count === 1) {
		if (inputText === '') {
			document.querySelector('#netLineCount>em').innerText = '0 lines';
		} else {
			document.querySelector('#netLineCount>em').innerText = count + ' line';
		}
	} else {
		document.querySelector('#netLineCount>em').innerText = count - 1 + ' lines';
	}
}
